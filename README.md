# Social-Network-Framework
This framework was designed for modeling attacks and detections on social networks. The framework exists with two implementations of the network, a node-based structure and a matrix-based structure.The framework also supports differet models for both attacks and detection.

## Authors
**Henry Xu** - University of California, Berkeley<br/>
**Frank Liao** - Carnegie Mellon University<br/>
**Jonathan Beltran** - Florida International University

## References
[Privacy Issues in Light of Reconnaissance Attacks with Incomplete Information](http://ieeexplore.ieee.org/document/7817068/?reload=true)<br/>
[Computational Social Networks](http://www.springer.com/mathematics/applications/journal/40649)
