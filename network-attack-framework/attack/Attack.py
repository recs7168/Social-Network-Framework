class Attack(object):
    def __init__(self, network, attackers):
        self.network = network
        self.attackers = []
        if type(attackers) is int:
            self.generateAttackers(attackers)
        elif type(attackers) is list:
            self.setAttackers(attackers)

    def generateAttackers(self, n):
        """randomly select n attackers"""
        return np.random.choice(self.network.getProfiles(), n)


    def setAttackers(self, attackers):
        """set the given profile ids to attackers"""
        self.attackers = attackers

    def getAttackers(self):
        """returns a list of profile ids for the attackers"""
        return self.attackers

    def attack(self):
        pass

    def __repr__(self):
        pass