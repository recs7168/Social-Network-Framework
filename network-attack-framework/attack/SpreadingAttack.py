from Attack import Attack
from networkFramework.MatrixNetwork import MatrixNetwork
import random, time, math
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import datetime
import time

class SpreadingAttack(Attack):
    class BestAttack:
        def __init__(self, target, bestAttacker, chance):
            self.target = target
            self.bestAttacker = bestAttacker
            self.chance = chance

    def __init__(self, network, attackers, features):
        super().__init__(network, attackers)
        # attack dict is a dict of dicts, profile -> attacker -> chance
        self.attackDict = dict()
        self.addedNodes = set()
        print("attackers:", attackers)
        self.mn = MatrixNetwork(network.nodeIds, connections=nx.to_numpy_matrix(network.connections))
        self.attackerCount = {attacker: 0 for attacker in attackers}
        self.features = features

    def attack(self, target, output=False):
        """PARAMS:
        probModel - function"""
        self.target = target
        self.setupAttack()
        
        nx.write_edgelist(self.network.connections, "before.csv")
        mostVulnerable = self.getOptimalAttack()
        while mostVulnerable.target != target:
            if mostVulnerable.chance == 0:
                print("\n\n")
                print("chosen target: ", target)
                print("failed to find target")
                return self.BestAttack(0, 0, 0), False
                
            it = time.time()
            
            for a in self.attackers: 
                self.attackDict[mostVulnerable.target][a] = -1
            #self.attackDict[mostVulnerable.target][mostVulnerable.bestAttacker] = -1
           
            self.addedNodes.add(mostVulnerable.target)
            self.attackerCount[mostVulnerable.bestAttacker] += 1;
            
            addSuccess = random.random() < mostVulnerable.chance
            if addSuccess:
                self.network.addConnection(mostVulnerable.target, mostVulnerable.bestAttacker)
                self.updateAttackDict(mostVulnerable.bestAttacker)
           
            self.printTarget(output, mostVulnerable, addSuccess)
                
            #print("time: " + str(time.time() - it))
            #print("\n")
            
            mostVulnerable = self.getOptimalAttack()
        
        print("\n\n")
        print("chosen target: ", target)
        self.printTarget(True, mostVulnerable, True)
        print(len(self.network.getConnections(self.target)))
        #for adversary in self.attackers:
        #    print("attacker", str(adversary) + ":", self.attackerCount[adversary])
        attackerSubgraph = nx.subgraph(self.network.connections,self.attackers)# adversary)
        
        self.printRes(self.network.connections, self.attackers)
        nx.write_edgelist(attackerSubgraph, "attacker_only.csv")
        nx.write_edgelist(self.network.connections, "after.csv")
        return mostVulnerable, True
        
    def printRes(self, network, attackers):
        for a in attackers:
            temp = set(attackers)
            temp.add(self.target)
            for friend in self.network.getConnections(a):
                temp.add(int(friend))
                
            color_dict = []
            width = []
            for i in temp:
                if i == a: 
                    color_dict += ["yellow"]
                elif i in attackers:
                    color_dict += ["red"]
                elif i == int(self.target):
                    color_dict += ["green"]
                else:
                    color_dict += ["blue"]
            
                
            subnetwork = network.subgraph(temp)
            
            for _ in subnetwork.edges():
                width += [0.1]
            
            plt.figure(1,figsize=(12,12)) 
            nx.draw(subnetwork, node_color = color_dict, with_labels = True, node_size=60, font_size=8, width=width)
            plt.savefig("frank" + str(a) + ".pdf")
            plt.clf()
    
    def compareFeatures(self, n1, n2):
        self.features[n1]
        self.features[n2]
        
   

    def printTarget(self, p, mostVulnerable, success):
        if p:
            print("\n")
            print("target: " + str(mostVulnerable.target))
            print("prob: " + str(mostVulnerable.chance))
            print("bestAttacker: " + str(mostVulnerable.bestAttacker))
            print("mutual:", str(len(self.network.getMutualConnections(mostVulnerable.bestAttacker, mostVulnerable.target))))
            print("outcome: " + str(success))
          
    def getOptimalAttack(self):
        """returns target, best attacker, and chance of attack"""
        maxValue = lambda d: max(d.values())
       
        target = max(self.attackDict.keys(), key=lambda k: (maxValue(self.attackDict[k]) > 0.2 and (100 - self.mn.getDistance(k, self.target) + maxValue(self.attackDict[k]))) or (10 - self.mn.getDistance(k, self.target) + maxValue(self.attackDict[k])))

        attackers = self.attackDict[target]
        bestAttacker =  max(attackers.keys(), key=lambda k: attackers[k])

        chance = attackers[bestAttacker]
            
        return self.BestAttack(target, bestAttacker, chance)

    def probModel(self, attacker, target):
        mutual = len(self.network.getMutualConnections(attacker, target))
        #chance = len(mutual) * 2 / max(len(self.network.getConnections(attacker)), len(self.network.getConnections(target)))
        #chance = math.log((len(mutual) + 1)/2) * 0.22805837 + 0.18014571
        #chance = random.random()
        
        if (((len(self.network.getConnections(attacker)) - 1) + (len(self.network.getConnections(target)) - 1) - mutual) != 0):
            chance = mutual / ((len(self.network.getConnections(attacker)) - 1) + (len(self.network.getConnections(target)) - 1) - mutual)
        else:
            return 1
        return chance

    def addTargetList(self, attacker, targetList):
        for target in targetList:
            tempProb = self.probModel(attacker, target);
            if target not in self.attackers and target not in self.addedNodes and self.attackDict[target][attacker] != -1 and tempProb > self.attackDict[target][attacker]:
                self.attackDict[target][attacker] = tempProb

    def updateAttackDict(self, attacker):
        potentialTargets = self.network.fastDegreeTwoStrong(attacker)
        self.addTargetList(attacker, potentialTargets)

    def setupAttack(self):
        # complete attacker network
        #for a1 in self.attackers:
            #for a2 in self.attackers:
                #if a1 != a2 and a2 not in self.network.getConnections(a1):
                    #self.network.addConnection(a1, a2)
        for i in self.network.nodeIds:
            if i not in self.attackers:
                self.attackDict[i] = dict()
                for a1 in self.attackers:
                    self.attackDict[i][a1] = 0
                    
        for a1 in self.attackers:
            potentialTargets = self.network.fastDegreeTwoStrong(a1)
            self.addTargetList(a1, potentialTargets)
        # create attack lists


    def writeAttack(self):
        connections = self.network.getConnections()
        date = datetime.date.today
        f = open('spreading-' + date.year + '-' + date.month + '-' + date.day + '-' + time.time() + '.txt', 'w')
        for attacker in self.attackers:
            f.write(str(attacker) + " ")
            f.write("\n")

        for p in connections:
            for f in connections[p]:
                f.write(str(p) + " " + str(f) + "\n")
        f.close()



ATTACKERS = 2

# a = {"a": {1: 1, 2: 2, 3: 0}, "b": {1: 3, 2: 2, 3: 0}}
#
# maxValue = lambda d: max(d.values())
#
# target = max(a.keys(), key=lambda k: maxValue(a[k]))
#
# maxKey = lambda d: max(d.keys(), key=lambda k: d[k])
# print(maxKey(a[target]))
