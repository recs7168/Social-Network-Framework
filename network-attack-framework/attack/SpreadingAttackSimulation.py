import facebook
from SpreadingAttack import SpreadingAttack
from networkFramework.SimpleNetwork import SimpleNetwork
import numpy as np
import networkx as nx

class SpreadingAttackSimulation:
	
    def __init__(self, n):
        facebook.load_network()
        self.nodes = list(range(4039))
        mn = SimpleNetwork(self.nodes, connections=facebook.network)
        self.attackers = list(np.random.choice(self.nodes, n, replace=False))
        self.result = SpreadingAttack(mn, self.attackers, facebook.feature_matrix())

    def attack(self, output = False):
        target = np.random.choice(self.nodes)
        while target in self.attackers:
            target = np.random.choice(self.nodes)
        return self.result.attack(target, output)

if __name__=="__main__":

    trials = 1
    rate = 0
    prob = 0
    for i in range(trials):
        sim = SpreadingAttackSimulation(4000)
        print("Starting Attack", i + 1)
        res, success = sim.attack(output=True)
        
        print("target: " + str(res.target))
        print("prob: " + str(res.chance))
        print("bestAttacker: " + str(res.bestAttacker))
        print("\n")
        
        print("success?", str(success))
        print("--------------------------------")
        
        if success:
            rate += 1
            prob += res.chance
            
    print("success rate:", str(rate / trials))
    print("avg probability:", str(prob / trials))
    
