import numpy as np
import scipy.linalg as la
import random
from networkFramework.Network import Network
from itertools import compress

class MatrixNetwork(Network):
    def __init__(self, nodeIds, nodeCount=None, connections=None):
        """OPTIONAL:
         nodeCount - number of nodes; for performance
         nodeConnections - dict of profiles to friends"""
        
        self.nodeIds = nodeIds
        self.nodeCount = (len(nodeIds) if nodeCount is None else nodeCount)

        self.connections = np.zeros((self.nodeCount, self.nodeCount)).astype(int)
        
        if np.any(connections):
            connections = np.array(connections)
            for x in range(0, self.nodeCount):
                for y in range(x, self.nodeCount):
                    self.connections[y][x] = self.connections[x][y] = connections[x][y]

        self.indices = dict()

        idx = 0
        for id in nodeIds:
            self.indices[id] = idx
            idx += 1

        self.memoizedDegrees = {}
        self.calculatedEig = False

    def getProfiles(self):
        return self.nodeIds

    def generateConnections(self, minChance, maxChance):
        for x in range(self.nodeCount):
            for y in range(self.nodeCount):
                chance = random.random()
                if chance < maxChance and chance > minChance:
                    addConnection(x, y)

    def setConnections(self, connections):
        """sets the connections of the network given a matrix of profiles to friends"""
        for x in range(0, self.nodeCount):
            for y in range(x, self.nodeCount):
                self.connections[y][x] = self.connections[x][y] = connections[x][y]

    def getConnections(self, profileId=None):
        """returns a dict of profiles to friends"""
        return self.getConnectionsFromMatrix(self.connections, profileId)
    
    def getConnectionsFromMatrix(self, matrix, profileId=None):
        if profileId:
            return self.getRealIds(matrix[self.indices[profileId]])
        else:
            res = dict()
            for id in self.nodeIds:
                res[id] = self.getRealIds(matrix[self.indices[id]])
                res[id].discard(id)
            return res

    def getRealIds(self, connectionSet):
        return set(compress(self.nodeIds, connectionSet))

    def getConnection(self, profileId, otherId):
        """returns the friendship value of the given profile ids"""
        return self.connections[profileId][otherId]

    def addConnection(self, profileId, otherId):
        self.connections[profileId][otherId] = self.connections[otherId][profileId] = 1

    def getMutualConnections(self, profile, otherId):
        other = self.getConnections(otherId)
        return [p for p in self.getConnections(profile) if p in other]

    def withinDegree(self, degree, profile, otherId):
        for i in range(degree, 0, -1):
            if otherId in self.getDistantConnections(i)[profile]:
                return True
        return False
        
    def getDistantConnections(self, degree, refresh=False):
        if not self.calculatedEig or degree not in self.memoizedDegrees or refresh:
            self.distantConnectionsHelper(degree, True)

        degreeConnections = self.getConnectionsFromMatrix(self.memoizedDegrees[degree])
        
        return degreeConnections
        
    def getDistantAltConnections(self, degree):
        if not self.calculatedEig or degree not in self.memoizedDegrees:
            self.distantConnectionsHelper(degree, False)

        degreeConnections = self.memoizedDegrees[degree]
        
        return degreeConnections
    

    def distantConnectionsHelper(self, degree, refresh):
        """caches connection tables from 1 to degree in self.memoizedDegrees""" 
        
        if not self.calculatedEig or refresh:
            connections = self.connections.copy()
            for i in range(len(self.nodeIds)):
                connections[i][i] = 0
            self.diag, self.P = la.eig(connections)
            self.pInverse = la.inv(self.P)
            self.calculatedEig = True
            self.memoizedDegrees[1] = self.getConnectionsFromMatrix(connections)
        
        for i in range(2, degree + 1):
            if refresh or not i in self.memoizedDegrees:
                DN = np.diag(np.round(self.diag**i, decimals=4))
                self.memoizedDegrees[i] = np.round(self.P.dot(DN).dot(self.pInverse))

    def fastDegreeTwoConnections(self):
        self.distantConnectionsHelper(2, True)
        return self.memoizedDegrees[2]


    def fastDegreeTwoStrong(self):
        connections = np.subtract(self.fastDegreeTwoConnections(), self.connections)
        return self.getConnectionsFromMatrix(connections)
    
    
    def getDistance(self, n1, n2):
        if n1 == n2:
            return 0;
        for i in range(2, 9):
            if self.getDistantAltConnections(i)[n1][n2]:
                return i
        return 1e20

    def __repr__(self):
        pass