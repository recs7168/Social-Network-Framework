class Network(object):
    def __init__(self, nodeIds, nodeCount=None, connections=None):
        """OPTIONAL:
         nodeCount - number of nodes; for performance
         nodeConnections - dict of profiles to friends"""

    def getProfiles(self):
        pass

    def generateConnections(self, minChance, maxChance):
        pass

    def setConnections(self, connections):
        """sets the connections of the network given a dict of profiles to friends"""
        pass

    def getConnections(self, profileId=None):
        """returns a dict of profiles to friends unless profileId specified
        then returns a set of friends"""
        pass

    def getConnection(self, profileId, otherId):
        """returns the friendship value of the given profile ids"""

    def addConnection(self, profileId, otherId):
        pass

    def getMutualConnections(self, profile, otherId):
        pass

    def getDistantConnections(self, degree, refresh=False):
        pass

    def writeConnections(self, fileName):
        pass

    def __repr__(self):
        pass