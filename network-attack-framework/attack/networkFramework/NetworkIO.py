import pickle

class NetworkIO:
	
	def write(self, network, filename):
		with open(filename, 'wb') as handle:
			pickle.dump(network, handle, protocol=pickle.HIGHEST_PROTOCOL)

	def read(self, filename):
		return pickle.load(open(filename, 'rb'))