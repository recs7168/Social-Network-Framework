from networkFramework.Network import Network
import random

class SimpleNetwork(Network):
    def __init__(self, nodeIds, nodeCount=None, connections=None):
        """OPTIONAL:
         nodeCount - number of nodes; for performance
         nodeConnections - dict of profiles to friends"""
        if not nodeCount:
            self.nodeCount = len(nodeIds)
        else:
            self.nodeCount = nodeCount

        self.nodeIds = nodeIds
        if not connections:
            self.setupConnections()
        else:
            self.connections = connections


    def getProfiles(self):
        return self.nodeIds

    def setupConnections(self):
        self.connections = dict()
        self.connections.add_nodes_from(self.nodeIds)

    def generateConnections(self, minChance, maxChance):
        for p in self.nodeIds:
            for f in self.nodeIds:
                if p != f and f not in self.connections[p]:
                    chance = random.random()
                    if chance < maxChance and chance > minChance:
                        self.addConnection(self, p, f)
    def setConnections(self, connections):
        """sets the connections of the network given a dict of profiles to friends"""
        self.connections = connections

    def getConnections(self, profileId):
        """returns a dict of profiles to friends, unless profileId is specified then return friends of given"""
        return self.connections[profileId]

    def getConnection(self, profileId, otherId):
        """returns the friendship value of the given profile ids"""
        return otherId in self.connections[profileId]

    def addConnection(self, profileId, otherId):
        self.connections.add_edge(profileId, otherId)

    def getMutualConnections(self, profile, otherId):
        return [p for p in self.getConnections(profile) if p in self.getConnections(otherId)]

    def getDistantConnections(self, degree, refresh=False):
        pass

    def __repr__(self):
        pass
        
    def fastDegreeTwoStrong(self, attacker):
        directConnections = self.getConnections(attacker)
        res = {p1 for p in directConnections for p1 in self.getConnections(p) if p1 not in directConnections}
        return res
        
    def zero(self, attacker):
        directConnections = self.getConnections(attacker)
        res = {p1 for p in directConnections for p1 in self.getConnections(p) if p1 not in directConnections}
        print(res)