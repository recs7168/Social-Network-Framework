from MatrixNetwork import MatrixNetwork as MN
import time
import numpy as np

class MatrixNetworkerTest(object):
    def __init__(self, nodeIds):
        t0 = time.time()
        print((time.time() - t0))
        self.network = MN(nodeIds)
        print((time.time() - t0))


    def test(self, friendChance):
        print("Starting Test")
        t0 = time.time()
        
        self.network.generateNetwork(friendChance)
        print("-" + str((time.time() - t0)))
        connections = self.network.getConnections()
        print("--" + str((time.time() - t0)))
        #print(connections)
        connectionsWoLabels = self.network.getConnections(labels=False)
        print("---" + str((time.time() - t0)))
        # print(connectionsWoLabels)
        # print(self.network.getDistantConnections(2))
        # print(self.network.getDistantConnections(2, bool=True))
        # print(self.network.getDistantFriends(1, 2, strong=False))

        ITERATIONS = 5
        temp1 = self.network.getDegreeFriends(1, 5)
        print(temp1)
        print((time.time() - t0) / ITERATIONS)
    

nodeIds = range(1, 1000)
test = MatrixNetworkerTest(nodeIds)
friendChance = (0.125, 0.5)
test.test(friendChance)



