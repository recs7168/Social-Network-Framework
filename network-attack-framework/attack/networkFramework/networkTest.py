import unittest
from MatrixNetwork import MatrixNetwork as MN
import numpy as np

class TestNetwork(unittest.TestCase):
    
    def setUp(self): 
        self.test_network1 = [[0, 1, 1, 0],
                            [1, 0, 1, 1],
                            [1, 1, 0, 1],
                            [0, 1, 1, 0]]

        self.test_network2 = [[0, 0, 0, 0], 
                            [0, 0, 0, 0],
                            [0, 0, 0, 0],
                            [0, 0, 0, 0]]


        self.test_network3 = [[1, 1, 1, 1],
                            [1, 1, 1, 1], 
                            [1, 1, 1, 1], 
                            [1, 1, 1, 1]]
        
        
        self.test_network4 = [[0, 1, 1, 1],
                            [1, 0, 1, 1], 
                            [1, 1, 0, 1], 
                            [1, 1, 1, 0]]


    def test_weak_distant_friends(self):

        #basic network test
        test_network = MN(np.arange(1, 5), connections = self.test_network1)
        self.assertEqual(test_network.getDistantConnections(2)[1], {2, 3, 4})

        #no connections
        test_network = MN(np.arange(1, 5), connections = self.test_network2)
        self.assertEqual(test_network.getDistantConnections(2)[1], set())
        
        #fully connected
        test_network = MN(np.arange(1, 5), connections = self.test_network4)
        self.assertEqual(test_network.getDistantConnections(2)[1], {2, 3, 4})

        #fully connected
        test_network = MN(np.arange(1, 5), connections = self.test_network3)
        self.assertEqual(test_network.getDistantConnections(2)[1], {2, 3, 4})


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestNetwork)
    unittest.TextTestRunner(verbosity=2).run(suite)
        